-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 14 Apr 2015 pada 16.03
-- Versi Server: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `pepeco`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `mcs`
--

CREATE TABLE IF NOT EXISTS `mcs` (
`id_mcs` int(15) NOT NULL,
  `judul_mcs` varchar(25) NOT NULL,
  `foto_mcs` varchar(255) NOT NULL,
  `data_mcs` varchar(255) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data untuk tabel `mcs`
--

INSERT INTO `mcs` (`id_mcs`, `judul_mcs`, `foto_mcs`, `data_mcs`) VALUES
(1, 'asdsad', 'Desert.jpg', 'dsewdsed'),
(2, 'hgjghjghj', 'bg-ac-milan-1.jpg', 'sdfsdf'),
(4, 'iopiopiopio', 'Red&Black.png', 'dsfsfsdf'),
(5, 'asdascvc', 'Hydrangeas.jpg', 'asdasd'),
(6, 'oor', 'oor.png', 'sadasdaz'),
(7, 'xcxzvcaaqq', 's.jpg', 'xccsssaaa');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pbf`
--

CREATE TABLE IF NOT EXISTS `pbf` (
`id_pbf` int(15) NOT NULL,
  `judul_pbf` varchar(25) NOT NULL,
  `foto_pbf` varchar(255) NOT NULL,
  `data_pbf` varchar(255) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data untuk tabel `pbf`
--

INSERT INTO `pbf` (`id_pbf`, `judul_pbf`, `foto_pbf`, `data_pbf`) VALUES
(1, 'asdasd', 'Jellyfish.jpg', 'jjljjljl'),
(2, 'xzcxzc', 'Chrysanthemum.jpg', 'zxcxzc'),
(3, 'zxcxzvc', 'Hydrangeas.jpg', 'xzczxcz'),
(4, 'xzbfgf', 'Tulips.jpg', 'asdasd'),
(5, 'asdasd', 'sad.png', 'as');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pcs`
--

CREATE TABLE IF NOT EXISTS `pcs` (
`id_pcs` int(15) NOT NULL,
  `judul_pcs` varchar(25) NOT NULL,
  `foto_pcs` varchar(255) NOT NULL,
  `data_pcs` varchar(255) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data untuk tabel `pcs`
--

INSERT INTO `pcs` (`id_pcs`, `judul_pcs`, `foto_pcs`, `data_pcs`) VALUES
(1, 'asdasd', 'Koala.jpg', 'klaklaklka'),
(2, 'asdasdxcx', 'Penguins.jpg', 'asdasda'),
(3, 'eewrwe', 'Jellyfish.jpg', 'zxczxc'),
(4, 'trttrerewe', 'Chrysanthemum.jpg', 'asdasdsa'),
(5, 'asdasdxxs', 'dx.jpg', 'qwdferewq');

-- --------------------------------------------------------

--
-- Struktur dari tabel `rccs`
--

CREATE TABLE IF NOT EXISTS `rccs` (
`id_rccs` int(15) NOT NULL,
  `judul_rccs` varchar(25) NOT NULL,
  `foto_rccs` varchar(255) NOT NULL,
  `data_rccs` varchar(255) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data untuk tabel `rccs`
--

INSERT INTO `rccs` (`id_rccs`, `judul_rccs`, `foto_rccs`, `data_rccs`) VALUES
(1, 'asdad', 'sa.jpg', 'psdiud'),
(2, 'asdascca', 'Koala.jpg', 'sadasd'),
(3, 'asdasdad', 'Jellyfish.jpg', 'sadasdasc'),
(4, 'asdasd', 'Tulips.jpg', 'assadasdacxc'),
(6, 'oor', 'images.jpg', 'okeokeok');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tcs`
--

CREATE TABLE IF NOT EXISTS `tcs` (
`id_tcs` int(15) NOT NULL,
  `judul_tcs` varchar(25) NOT NULL,
  `foto_tcs` varchar(255) NOT NULL,
  `data_tcs` varchar(255) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data untuk tabel `tcs`
--

INSERT INTO `tcs` (`id_tcs`, `judul_tcs`, `foto_tcs`, `data_tcs`) VALUES
(3, 'zxczxczc', 'as.jpg', 'jlflhlflhlf'),
(4, 'Koala', 'Koala.jpg', 'koala ini sedang tersenyum'),
(5, 'sfdvv', 'Desert.jpg', 'dssrrtt'),
(6, 'asdxda', 'Tulips.jpg', 'tluppp'),
(7, 'JellyFish', 'Jellyfish.jpg', 'Ini adalah jellyfish sepesies langka yang jarang ditemukan manusia ..'),
(8, 'logo', 'Red&Black.png', 'logo merah');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE IF NOT EXISTS `user` (
`id_user` int(15) NOT NULL,
  `username` varchar(25) NOT NULL,
  `password` varchar(25) NOT NULL,
  `level` varchar(15) NOT NULL,
  `jenis_kelamin` varchar(25) NOT NULL,
  `nm_lengkap` varchar(25) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `email` varchar(25) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id_user`, `username`, `password`, `level`, `jenis_kelamin`, `nm_lengkap`, `alamat`, `email`) VALUES
(1, 'admin', 'admin1', 'admin', 'Laki Laki', 'Admin', 'Jl.AdminSelaluBenar', 'Admin@aist.co.id'),
(5, 'user', 'user', 'user', 'Laki-Laki', 'user', 'Jl.UserBiasaSaja', 'user@aist.co.id'),
(8, 'dummy', 'dummy', 'user', 'Perempuan', 'dummy', 'Jl.Dummy', 'dummy@ads');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `mcs`
--
ALTER TABLE `mcs`
 ADD PRIMARY KEY (`id_mcs`);

--
-- Indexes for table `pbf`
--
ALTER TABLE `pbf`
 ADD PRIMARY KEY (`id_pbf`);

--
-- Indexes for table `pcs`
--
ALTER TABLE `pcs`
 ADD PRIMARY KEY (`id_pcs`);

--
-- Indexes for table `rccs`
--
ALTER TABLE `rccs`
 ADD PRIMARY KEY (`id_rccs`);

--
-- Indexes for table `tcs`
--
ALTER TABLE `tcs`
 ADD PRIMARY KEY (`id_tcs`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
 ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `mcs`
--
ALTER TABLE `mcs`
MODIFY `id_mcs` int(15) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `pbf`
--
ALTER TABLE `pbf`
MODIFY `id_pbf` int(15) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `pcs`
--
ALTER TABLE `pcs`
MODIFY `id_pcs` int(15) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `rccs`
--
ALTER TABLE `rccs`
MODIFY `id_rccs` int(15) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tcs`
--
ALTER TABLE `tcs`
MODIFY `id_tcs` int(15) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
MODIFY `id_user` int(15) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
