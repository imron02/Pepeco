<?php
	include "koneksi.php";
	error_reporting(0);
	
	$id_user = $_GET['id_user'];
	
	$sql = "SELECT * FROM user WHERE id_user='$id_user'";
	$eksekusi = mysql_query($sql);
	
	$tampil = mysql_fetch_array($eksekusi);
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>PT.PEPECO</title>
	<link rel="stylesheet" href="../css/bootstrap.min.css">
</head>
<body>
	<div class="container">
		<div class="col-md-4"></div>
		<div class="col-md-4">
			<div class="panel panel-primary">
			<div class="panel-heading">Daftar User PEPECO</div>
			<div class="panel-body">
				<form method="post" action="edit_user.php">
					<div class="form-group">
						<input type="hidden" class="form-control" id="exampleInputUsername1" name="id_user" value="<?php echo $tampil['id_user'];?>">
					</div>
				  <div class="form-group">
				    <label for="exampleInputUsername1">Username</label>
				    <input type="text" class="form-control" id="exampleInputUsername1" placeholder="Username" name="username" 
				    value="<?php echo $tampil['username'];?>" required>
				  </div>
				  <div class="form-group">
				    <label for="exampleInputPassword1">Password</label>
				    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" name="password"
				    value="<?php echo $tampil['password'];?>" required>
				  </div>
				  <div class="form-group">
				    <label for="exampleInputName1">Nama Lengkap</label>
				    <input type="text" class="form-control" id="exampleInputName1" placeholder="Nama Lengkap" name="nm_lengkap" 
				    value="<?php echo $tampil['nm_lengkap'];?>" required>
				  </div>
				  <div class="form-group">
				    <label for="exampleInputEmail1">E-mail</label>
				    <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Email" name="email" 
				    value="<?php echo $tampil['email'];?>" required>
				  </div>
				  <div class="form-group">
				    <label for="exampleInputLevel1">Level</label>
					    <select class="form-control" name="level">
						  <option value="admin">Admin</option>
						  <option value="user">User</option>
						</select>
				  </div>
				  <div class="form-group">
				  	<label for="exampleInputjns_klm1">Jenis Kelamin</label><br>
				  	<label class="radio-inline">
					<input type="radio" name="jenis_kelamin" id="inlineRadio2" value="Laki-Laki"> Laki-Laki
					</label><br>
				  	<label class="radio-inline">
					<input type="radio" name="jenis_kelamin" id="inlineRadio2" value="Perempuan"> Perempuan 
				  	</label>
				  </div>
				  <div class="form-group">
				  <label for="exampleInputAlamat1">Alamat</label>
				  <textarea class="form-control" rows="2" name="alamat" required><?php echo $tampil['alamat'];?></textarea>
				  </div>
				  <button type="submit" class="btn btn-primary" style="float:right;">Simpan</button>
				  <button type="Reset" class="btn btn-primary">Ulangi</button>
				</form>
			</div>
			</div>
		</div>
		<div class="col-md-4"></div>
	</div>
</body>
</html>