<?php
	include"navbar.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>PEPECO</title>
	<link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>
	<div class="container">
		<div class="col-md-1"></div>
		<div class="col-md-10">
			<div class="panel panel-default" style="margin-top:10%;">
			  
			    <table class="table table-striped">
					<tr>
						<th>NO</th>
						<th>Jenis Produk</th>
						<th style="text-align:center;">Option</th>
					</tr>
					<tr>
						<td style="padding:15px;">1</td>
						<td style="padding:15px;">TCS - Temperature Controlled Storage</td>
						<td style="width:140px;"><a href="tcs/tambah_tcs.php"><button type="button" class="btn btn-primary btn-sm">Upload</button></a>
							<a href="tcs/show_tcs.php"><button type="button" class="btn btn-primary btn-sm">View</button></a></td>
					</tr>
					<tr>
						<td style="padding:15px;">2</td>
						<td style="padding:15px;">RCCS - Refrigerated Container And Cold Storage</td>
						<td style="width:140px;"><a href="rccs/tambah_rccs.php"><button type="button" class="btn btn-primary btn-sm">Upload</button></a>
							<a href="rccs/show_rccs.php"><button type="button" class="btn btn-primary btn-sm">View</button></a></td>
					</tr>
					<tr>
						<td style="padding:15px;">3</td>
						<td style="padding:15px;">PBF - Portable Blast Freezers</td>
						<td style="width:140px;"><a href="pbf/tambah_pbf.php"><button type="button" class="btn btn-primary btn-sm">Upload</button></a>
							<a href="pbf/show_pbf.php"><button type="button" class="btn btn-primary btn-sm">View</button></a></td>
					</tr>
					<tr>
						<td style="padding:15px;">4</td>
						<td style="padding:15px;">PCS - Pharmaceutical Cold Stores</td>
						<td style="width:140px;"><a href="pcs/tambah_pcs.php"><button type="button" class="btn btn-primary btn-sm">Upload</button></a>
							<a href="pcs/show_pcs.php"><button type="button" class="btn btn-primary btn-sm">View</button></a></td>
					</tr>
					<tr>
						<td style="padding:15px;">5</td>
						<td style="padding:15px;">MCS - Mega Cold stores</td>
						<td style="width:140px;"><a href="mcs/tambah_mcs.php"><button type="button" class="btn btn-primary btn-sm">Upload</button></a>
							<a href="mcs/show_mcs.php"><button type="button" class="btn btn-primary btn-sm">View</button></a></td>
					</tr>
				</table>
			  
			</div>
		</div>
		<div class="col-md-1"></div>
	</div>
</body>
</html>