<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>PT.PEPECO</title>
	<link rel="stylesheet" href="../css/bootstrap.min.css">
</head>
<body>
	<div class="container">
		<div class="col-md-4"></div>
		<div class="col-md-4">
			<div class="panel panel-primary" style="margin-top:10%;">
			<div class="panel-heading">Daftar User PEPECO</div>
			<div class="panel-body">
				<form method="post" action="proses_daftar.php">
				  <div class="form-group">
				    <label for="exampleInputUsername1">Username</label>
				    <input type="text" class="form-control" id="exampleInputUsername1" placeholder="Username" name="username" required>
				  </div>
				  <div class="form-group">
				    <label for="exampleInputPassword1">Password</label>
				    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" name="password" required>
				  </div>
				  <div class="form-group">
				    <label for="exampleInputName1">Nama Lengkap</label>
				    <input type="text" class="form-control" id="exampleInputName1" placeholder="Nama Lengkap" name="nm_lengkap" required>
				  </div>
				  <div class="form-group">
				    <label for="exampleInputEmail1">E-mail</label>
				    <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Email" name="email" required>
				  </div>
				  <div class="form-group">
				  	<label for="exampleInputjns_klm1">Jenis Kelamin</label><br>
				  	<label class="radio-inline">
					<input type="radio" name="jenis_kelamin" id="inlineRadio2" value="Laki-Laki"> Laki-Laki
					</label><br>
				  	<label class="radio-inline">
					<input type="radio" name="jenis_kelamin" id="inlineRadio2" value="Perempuan"> Perempuan 
				  	</label>
				  </div>
				  <div class="form-group">
				  <label for="exampleInputAlamat1">Alamat</label>
				  <textarea class="form-control" rows="2" name="alamat" required></textarea>
				  </div>
				  <button type="submit" class="btn btn-primary" style="float:right;">Simpan</button>
				  <button type="Reset" class="btn btn-primary">Ulangi</button>
				</form>
			</div>
			</div>
		</div>
		<div class="col-md-4"></div>
	</div>
</body>
</html>